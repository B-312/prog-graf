# Programación Gráfica

Esta es una asignatura Optativa que contribuye a la formación Profesional del estudiante. En la misma se desarrollarán los siguientes temas: Introducción a la programación gráfica, Aplicaciones, clasificación y características de la
programación gráfica, Algoritmos gráficos, Transformaciones geométricas Bidimensionales, Programa de graficación, Desarrollo de interfaces de usuario para web y App, Conocer los diferentes
librerías (framework) para **interfaces de usuario (UI)** y **experiencia del usuario (UX)**.

## OBJETIVOS GENERALES:

- Distinguir las principales técnicas de diseño y generación de gráficas por computadora.
- Diseñar un sistema de graficación con primitivas gráficas creadas por el alumno.
- Desarrollar en diferentes plataforma interfaces de usuario (UI) web y App.


## OBJETIVOS ESPECÍFICOS:
- Determinar los conceptos principales para realizar interfaces usuarios para distintos dispositivos.
- Describir los métodos de desplegado de gráficas y la tecnología que sustenta la graficación por computadoras.
- Evaluar el impacto que tiene en la sociedad los sistemas de información visuales con interfaces amigable.
- Identificar los métodos de generación de rectas y curvas en el plano virtual de dos dimensiones que representa la pantalla de video.
- Explicar los diferentes librerías (framework) para el desarrollo de interfaces de usuario y App.


## Recursos del curso
- Libreria para interfaces gráfica Web **[element](https://element.eleme.io/#/es/component/)**
- Libreria para interfaces gráfica App **[onsen-ui](https://onsen.io/v2/guide/)**